<?php

return [
    'Id'  =>  'ID',
    'Title'  =>  '标题',
    'Content'  =>  '内容',
    'Sort'  =>  '排序',
    'Showtime'  =>  '显示的时间',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
