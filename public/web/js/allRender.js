var allFun = {
    getData: function (url, sendData, callback) {
        $.post(url, sendData, function (res) {
            callback.call(this, res);
        });
    },
    renderMenu: function (obj , callBack) {
        //菜单加载
        allFun.getData(RouteApi.getMenuApi, {}, function (res) {
            var str = '';
            var secondStr = '';
            var thirdStr = '';
            var ulThirdPre = '<ul class="abs-right fl">';
            var ulThirdLast = '</ul>';
            var bool = false;
            for(i = 0 , len = res.data.length ; i < len ; i++){
                if(res.data[i].child.length != 0){
                    secondStr = '';
                    thirdStr = '';
                    bool = false;
                    for(j = 0 , lenJ = res.data[i].child.length ; j < lenJ ; j++){
                        secondStr += `<li class="abs-left-title"><a class="abs-title-a" href="${res.data[i].child[j].url}">${res.data[i].child[j].name}</a></li>`;
                        if(res.data[i].child[j].about){
                            bool = true;
                            thirdStr += `<li class="abs-right-li">
                                <img src="${res.data[i].child[j].about.image}" alt="" class="abs-right-img">
                                <p class="abs-right-desc u-ells3">${res.data[i].child[j].about.depict}</p>
                              </li>`;
                        }
                    }
                    if(bool){
                        thirdStr = ulThirdPre +  thirdStr + ulThirdLast;
                        var className = 'nav-li-about';
                    }else{
                        var className = '';
                    }
                    str += `<li class="nav-li ${className}">
                          <a class="nav-a" href="javascript:void(0)">${res.data[i].name}</a>
                          <img class="nav-li-img nav-li-img-bottom " src="./web/images/3.png" alt="" class="nav-li-icon">
                          <img class="nav-li-img nav-li-img-top" src="./web/images/2.png" alt="" class="nav-li-icon">
                          <div class="abs clear">
                            <ul class="abs-left fl">
                              ${secondStr}
                            </ul>
                            ${thirdStr}
                          </div>
                        </li>`;
                }else {
                    str += `<li class="nav-li"><a class="nav-a nav-li-actived" href="${res.data[i].url}">${res.data[i].name}</a></li>`;
                }
            }
            $(obj).empty().append(str);
            callBack.call(this);

        });
       
    },
    renderBeian:function(obj){
        //备案设置
        allFun.getData(RouteApi.getBeianApi, {}, function (res) {
            if(res.data.beian_url.length != 0){
                var str = `<a href="${res.data.beian_url.value}">${res.data.beian.value}</a>`;
            }else{
                var str = res.data.beian.value;
            }
            $(obj).html(str);
        });
    },
    renderAbout: function (obj1, obj2) {
        //公司简介和关于我们
        allFun.getData(RouteApi.getAboutApi, {}, function (res) {
            var str = `
                <div class="about-aside fl">
              <div class="aboutItem fl">
                <h1 class="aboutItem-title about-title-re" >${res.data[0].hasmenu.name}</h1>
                <div class="aboutItem-content">${res.data[0].depict}</div>
                <a href="${res.data[0].hasmenu.url}" class="aboutItem-btn">more+</a>
              </div>
              <img class="about-left-img fl" src="${res.data[0].image}" alt="">
              <img class="about-left-img fl" src="${res.data[1].image}" alt="">
              <div class="aboutItem fl">
                <h1 class="aboutItem-title about-title-re" >${res.data[1].hasmenu.name}</h1>
                <div class="aboutItem-content">${res.data[1].depict}</div>
                <a href="${res.data[1].hasmenu.url}" class="aboutItem-btn">more+</a>
              </div>
            </div>
            <div class="about-aside fl">
              <div class="aboutItem" style="position: absolute;">
                <h1 class="aboutItem-title about-title-re">${res.data[2].hasmenu.name}</h1>
                <div class="aboutItem-content">${res.data[2].depict}</div>
                <a href="${res.data[2].hasmenu.url}" class="aboutItem-btn">more+<a>
              </div>
              <img class="about-right-img" src="${res.data[2].image}" alt="">
            </div>`;

            var abstractStr = `<ul class="brief-title-ul">
                      <li class="brief-title-li brief-title-li-selected">${res.data[3].hasmenu.name}</li>
                    </ul>
                    <img src="${res.data[3].image}" alt="" class="brief-left-img">
                    <p class="brief-left-desc u-ells5">${res.data[3].depict}</p>      
                    <div class="brief-btn-ul clear">
                      <a href="${res.else.url}"><li class="brief-bottom-btn contact fr">联系我们</li></a>
                      <a href="${res.data[3].hasmenu.url}"><li class="brief-bottom-btn more fr">了解更多</li></a>
                    </div>`;
            $(obj1).empty().append(str);
            $(obj2).empty().append(abstractStr);
        });
    },
    renderFriend: function (obj) {
        //友情链接
        allFun.getData(RouteApi.getFriendApi, {}, function (res) {
            var str = '';
            for (i = 0 , len = res.data.length; i < len; i++) {
                try {
                    let href = (res.data[i].url.length == 0) ? 'javascript:void(0);' : res.data[i].url;
                    str += `<li class="link-li">
                            <a href="${href}"><img src="${res.data[i].image}" alt="">
                            </a>
                        </li>`;
                } catch (e) {

                }
            }
            $(obj).append(str);
        });
    },
    renderContact: function (obj1, obj2) {
        //联系我们
        allFun.getData(RouteApi.getContactApi, {}, function (res) {
            var str = '';
            var qrcode = '';
            if (res.data.tel.length != 0) {
                str += `<li class="footer-li">
                    <img class="footer-icon" src="./web/images/21.png" alt="">
                    <a href="tel:${res.data.tel}" class="footer-link">电话：${res.data.tel}</a>
                    </li>`;
            }

            if (res.data.fax.length != 0) {
                str += `<li class="footer-li">
                    <img class="footer-icon" src="./web/images/22.png" alt="">
                    <a href="" class="footer-link">传真：${res.data.fax}</a>
                    </li>`;
            }
            if (res.data.zip_code.length != 0) {
                str += `<li class="footer-li">
                    <img class="footer-icon" src="./web/images/23.png" alt="">
                    <a href="mailto:${res.data.zip_code}" class="footer-link">邮箱：${res.data.zip_code}</a>
                    </li>`;
            }
            if (res.data.web_site.length != 0) {
                str += `<li class="footer-li">
                    <img class="footer-icon" src="./web/images/24.png" alt="">
                    <a href="${res.data.web_site}" class="footer-link">网址：${res.data.web_site}</a>
                    </li>`;
            }
            if (res.data.qrcode.length != 0) {
                qrcode += `<img class="" src="${res.data.qrcode}" alt="">`;
            }
            $(obj1).append(str);
            $(obj2).append(qrcode);
        });
    },
    renderLogo: function (obj) {
        //logo设置
        allFun.getData(RouteApi.getLogoApi, {}, function (res) {
            $(obj).attr('src', res.data.value);
        });
    },
    renderLawList: function (obj1, obj2) {
        //资料文库列表
        allFun.getData(RouteApi.getLawListApi, {}, function (res) {
            var str = '';
            var lawStr = '';
            for (i = 0 , len = res.data.length; i < len; i++) {
                str = `<li class="levelOne-li" id="catelog-${res.data[i].id}">${res.data[i].name}</li>`;
                $(obj1).append(str);
                var lawStr = '';
                for (j = 0 , lenJ = res.data[i].hasmanylaw.length; j < lenJ; j++) {
                    lawStr += `<li class="law-li clear">
                        <div class="fl law-li-imgBox">
                          <img class="law-li-img" src="${res.data[i].hasmanylaw[j].image}" alt="">
                        </div>
                        <div class="fr law-right">
                          <h1 class="law-li-title u-ell">${res.data[i].hasmanylaw[j].title}</h1>
                          <p class="law-li-desc u-ells3">${res.data[i].hasmanylaw[j].introduct}</p>
                          <div class="clear">
                            <time class="law-li-time fl">${timeClass.getDate(res.data[i].hasmanylaw[j].showtime)}</time>
                            <a href="{:url('')}" class="law-li-btn fr">查看详情</a>
                          </div>
                        </div>
                      </li>`;
                }
                $(obj2).append(`<ul class="law-ul clear" id="content-${res.data[i].id}">` + lawStr +  `</ul>`);
            }
            initList();
        });
    },
    renderLaw: function (sendData, obj1) {
        //（法律）文章具体内容
        allFun.getData(RouteApi.getLawApi, sendData, function (res) {
            var str = `<h1 class="lawDtl-title">${res.data.detail.title}</h1>
              <ul class="author clear">
                <li>作者：${res.data.detail.author}</li>
                <li>时间：${timeClass.getDate(res.data.detail.showtime)}</li>
                <li>来源：${res.data.detail.source_name}</li>
              </ul>
              <div class="desc">
                <p>${res.data.detail.content}</p>
              </div>
              <div class="nextArticle clear">
                <ul class="fl">
                  <li>
                    <a onclick="lawDtlClass.turnPage(this)" data-id="${res.data.list.last.id}">上一篇：${res.data.list.last.title}</a>
                  </li>
                  <li>
                    <a onclick="lawDtlClass.turnPage(this)" data-id="${res.data.list.next.id}">下一篇：${res.data.list.next.title}</a>
                  <li>
                </ul>
                <a class="nextArticle-back fr" onClick="javascript :history.back(-1);">返回上一页</>
              </div>`;
            $(obj1).empty().append(str);
        });
    },
    renderService: function (sendData, obj1, obj2, obj3, callBack) {
        //服务项目加载
        allFun.getData(RouteApi.getSeriveApi, sendData, function (res) {
            var str = '';
            var secondStr = '';
            var thirdStr = '';
            var ul_pre_one = `<ul class="levelTwo-ul clear">`;
            var ul_last_one = `</ul>`;


            for (i = 0 , len = res.data.length; i < len; i++) {
                str += `<li class="levelOne-li" id="catelog-${res.data[i].id}">${res.data[i].name}</li>`;
                secondStr = '';
                thirdStr = '';
                for (j = 0 , lenJ = res.data[i].service.length; j < lenJ; j++) {
                    secondStr += `<li class="levelTwo-li" id="catelog-${res.data[i].service[j].id}">
                        <div class="levelTwo-a">${res.data[i].service[j].name}</div>
                    </li>`;
                    thirdStr += `<li class="service-desc-li" id="content-${res.data[i].service[j].id}">
                                <p>${res.data[i].service[j].content}</p>
                            </li>`;

                }
                $(obj2).append(ul_pre_one + secondStr + ul_last_one);
                $(obj3).append(` <ul class="service-desc-ul" id="content-${res.data[i].id}">` + thirdStr + `</ul>`);
            }
            $(obj1).empty().append(str);
            callBack.call(this);
        });
    },
    renderDynamic: function (sendData, obj1, obj2, callBack) {
        //监控动态加载
        allFun.getData(RouteApi.getDynamicApi, sendData, function (res) {
            var str = '';
            var contentstr = '';
            for (i = 0 , len = res.data.length; i < len; i++) {
                if (!res.data[i].dynamic) {
                    dynamicstr = '';
                } else {
                    dynamicstr = res.data[i].dynamic['content'];
                }

                str += `<li class="levelOne-li" id="catelog-${res.data[i].id}">${res.data[i].name}</li>`;

                contentstr += `<div class="lawDtl law-ul " id="content-${res.data[i].id}"><div class="desc">
                            <p>${dynamicstr}</p>
                          </div>
                        </div>`;
            }
            $(obj1).empty().append(str);
            $(obj2).empty().append(contentstr);
            callBack.call(this);
        });
    },
    renderIndexService: function (sendData, obj) {
        //首页的服务项目
        allFun.getData(RouteApi.getIndexServiceApi, sendData, function (res) {
            var str = '';
            var aStr = '';
            for (i = 0 , len = res.data.length; i < len; i++) {
                aStr = '';
                for (j = 0 , lenJ = res.data[i].service.length; j < lenJ; j++) {
                    aStr += `<a href="${WebRoute.service + '?one='+res.data[i].id + '&two='+res.data[i].service[j].id}"> ${res.data[i].service[j].name}</a>`;
                }
                str += `<li class="service-li">
                    <img class="service-img" src="${res.data[i].icon}" alt="">
                    <h4 class="service-title">${res.data[i].name}</h4>
                    <div class="service-abs">
                      <h2 class="service-abs-title u-ell">${res.data[i].name}</h2>
                      <div class="service-abs-link">
                      ${aStr}
                      </div>
                    </div>
                  </li>`;
            }
            $(obj).empty().append(str);
        });
    },
    renderArticleList: function (sendData, obj1 , obj2, callBack) {
        allFun.getData(RouteApi.getArticleListApi, sendData, function (res) {
            var typeArr = {1:'公司动态',2:'行业新闻',3:'培训计划',4:'客户案例'};
            var menuStr = '';
            var str = '';
            var secondStr = '';
            var ul_pre = '<ul class="news">';
            var ul_last = '</ul>';

            for(i = 0 , len = res.data.length; i < len ; i++){
                secondStr = '';
                for(j = 0 , lenJ = res.data[i].length; j < lenJ ; j++){
                    secondStr += `<li class="news-li clear">
                            <time class="news-left fl">
                            <p class="news-time-monthDay news-time news-time-date ">${timeClass.getMonth(res.data[i][j].showtime)}-${timeClass.getDay(res.data[i][j].showtime)}</p>
                            <p class="news-time news-time-year">${timeClass.getYear(res.data[i][j].showtime)}</p>
                            </time>
                            <div class="news-right fr" style="cursor: pointer;">
                            <a href="${WebRoute.contentShow + '?article_id=' + res.data[i][j].id}" target="_blank">
                                <h1 class="news-title u-ell">${res.data[i][j].title}</h1>
                                <p class="news-content u-ells">${res.data[i][j].depict}</p>
                            </a>
                            </div>
                        </li>`;
                    var typeId = res.data[i][j].type;
                }
                str += ul_pre+secondStr+ul_last;
                menuStr += `<li class="brief-title-li" data-type="${typeId}">${typeArr[typeId]}</li>`;
            }
            $(obj1).empty().append(str);
            $(obj2).empty().append(menuStr);
            callBack.call(this);
        });
    },
    renderAricleShow:function (sendData , typeName , obj , callBack) {
        var ApiUrl = RouteApi.getArticleShowApi;
        if(typeName == 'article'){
            ApiUrl = RouteApi.getArticleShowApi;
        }else if(typeName == 'about'){
            ApiUrl = RouteApi.getAboutDetailApi;
        }else if(typeName == 'contact'){
            ApiUrl = RouteApi.getContactDetailApi;
        }

        allFun.getData(ApiUrl, sendData, function (res) {
            if(typeName == 'about'){
                var str = `<h1 class="lawDtl-title">${res.data.detail.hasmenu.name}</h1>
                      <ul class="author clear" >
                      </ul>
                      <div class="desc">
                        <p>${res.data.detail.content}</p>
                      </div>
                      <div class="nextArticle clear">
                      </div>`;
            }else if(typeName == 'contact'){
                var str = `<h1 class="lawDtl-title">联系我们</h1>
                      <ul class="author clear" >
                      </ul>
                      <div class="desc">
                        <p>${res.data.detail.content}</p>
                      </div>
                      <div class="nextArticle clear">
                      </div>`;
            }else{
                var str = `<h1 class="lawDtl-title">${res.data.detail.title}</h1>
                      <ul class="author clear" >
                        <li style="float: right;">时间：${timeClass.getDate(res.data.detail.showtime)}</li>
                      </ul>
                      <div class="desc">
                        <p>${res.data.detail.content}</p>
                      </div>
                      <div class="nextArticle clear">
                        <ul class="fl">
                          <li>
                            <a onclick="lawDtlClass.turnPage(this)" data-id="${res.data.list.last.id}">上一篇：${res.data.list.last.title}</a>
                          </li>
                          <li>
                            <a onclick="lawDtlClass.turnPage(this)" data-id="${res.data.list.next.id}">下一篇：${res.data.list.next.title}</a>
                          <li>
                        </ul>
                      </div>`;
            }

            $(obj).empty().append(str);
        });
    },
    renderContentList: function (sendData , obj1, obj2 , callBack) {
        //其他设置列表
        allFun.getData(RouteApi.getArticleListByTypeApi, sendData, function (res) {
            var typeArr = {1:'公司动态',2:'行业新闻',3:'培训计划',4:'客户案例'};
            var str = '';
            var listStr = '';
            var ul_pre = `<ul class="law-ul clear">`;
            var ul_last = `</ul>`;
            for (i = 0 , len = res.data.length; i < len; i++) {
                str = `<li class="levelOne-li" data-type="${res.data[i].type}">${typeArr[res.data[i].type]}</li>`;
                listStr += `<li class="law-li clear">
                        <div class="fl law-li-imgBox">
                          <img class="law-li-img" src="${res.data[i].image}" alt="">
                        </div>
                        <div class="fr law-right">
                          <h1 class="law-li-title u-ell">${res.data[i].title}</h1>
                          <p class="law-li-desc u-ells3">${res.data[i].depict}</p>
                          <div class="clear">
                            <time class="law-li-time fl">${timeClass.getDate(res.data[i].showtime)}</time>
                            <a href="${WebRoute.contentShow +'?article_id=' + res.data[i].id}" class="law-li-btn fr">查看详情</a>
                          </div>
                        </div>
                      </li>`;
            }
            $(obj1).empty().append(str);
            $(obj2).append(ul_pre + listStr + ul_last);
            callBack.call(this);
        });
    },
    renderServiceInfo : function(sendData , obj ,callBack){
        //客服信息加载
        allFun.getData(RouteApi.getServiceInfoApi, sendData, function (res) {
            var str = `<h2 class="fixed-info-title" >${res.data.service_list_name}<img onclick="fixedBox()" src="/web/images/qq/icon4.png" alt=""></h2>
                <div class="fixed-info-content">
                    <div class="fixed-info-qq">
                        <a class="fixed-info-qq-box"
                              title="点击这里给我发消息"
                              href="http://wpa.qq.com/msgrd?v=3&amp;uin=${res.data.qq_1}&amp;site=qq&amp;menu=yes"
                              target="_blank">
                              <img src="/web/images/qq/icon1.png">
                              <span>客服一</span>
                        </a>
                        <a class="fixed-info-qq-box"
                              title="点击这里给我发消息"
                              href="http://wpa.qq.com/msgrd?v=3&amp;uin=${res.data.qq_2}&amp;site=qq&amp;menu=yes"
                              target="_blank">
                              <img src="/web/images/qq/icon1.png">
                              <span>客服二</span>
                            </a>
                        </div>
                        <div class="fixed-code">
                          <img src="${res.data.qrcode_image}">
                          <p>扫码关注</p>
                        </div>
                        <div class="fixed-word">
                        <h3>${res.data.hotline_name}</h3>
                        <h4>${res.data.hotline_1}</h4>
                        <h4>${res.data.hotline_2}</h4>
                     </div>
                </div>`;

            $(obj).empty().append(str);
        });
    }
};