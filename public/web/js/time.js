var timeClass = {
    //时间戳转日期
    getDate:function(timestamp){
        var time = new Date(timestamp * 1000);
        var year = time.getFullYear();
        var month = time.getMonth()+1;
        var date = time.getDate();
        var hour = time.getHours();
        var minute = time.getMinutes();
        var second = time.getSeconds();
        return year+"-"+month+"-"+date;
    },
    getYear : function (timestamp) {
        var time = new Date(timestamp * 1000);
        return time.getFullYear();
    },
    getMonth : function (timestamp) {
        var time = new Date(timestamp * 1000);
        var month = time.getMonth() + 1;
        if(month < 10){
            month = '0' + month;
        }
        return month;
    },
    getDay : function (timestamp) {
        var time = new Date(timestamp * 1000);
        return time.getDate();
    },

};