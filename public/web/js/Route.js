var locationSite = 'http://'+window.location.host;
var locationSite1 = 'http://ast.seedian.com.cn';
var RouteApi = {
    'getMenuApi' : locationSite + '/api/getMenuApi',
    'getBannerApi' : locationSite + '/api/getBannerApi',
    'getAboutApi' : locationSite + '/api/getAboutApi',
    'getFriendApi' : locationSite + '/api/getFriendApi',
    'getContactApi' : locationSite + '/api/getContactApi',
    'getLogoApi' : locationSite + '/api/getLogoApi',
    'getLawListApi' : locationSite + '/api/getLawListApi',
    'getLawApi' : locationSite + '/api/getLawApi',
    'getSeriveApi' : locationSite + '/api/getSeriveApi',
    'getDynamicApi' : locationSite + '/api/getDynamicApi',
    'getIndexServiceApi' : locationSite + '/api/getIndexServiceApi',
    'getArticleListApi' : locationSite + '/api/getArticleListApi',
    'getArticleShowApi' : locationSite + '/api/getArticleShowApi',
    'getArticleListByTypeApi' : locationSite + '/api/getArticleListByTypeApi',
    'getAboutDetailApi' : locationSite + '/api/getAboutDetailApi',
    'getContactDetailApi' : locationSite + '/api/getContactDetailApi',
    'getBeianApi' : locationSite + '/api/getBeianApi',
    'getServiceInfoApi' : locationSite + '/api/getServiceInfoApi',//客服信息
};

var WebRoute = {
    'index' : locationSite + '/index.html',
    'service' : locationSite + '/service.html',
    'contentShow' : locationSite + '/contentShow.html',
    'contentList' : locationSite + '/contentList.html',
    'lawDtl' : locationSite + '/lawDtl.html',
};