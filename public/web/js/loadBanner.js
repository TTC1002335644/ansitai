var loadBanner = {
    getBannerData : () => {
        $.post(RouteApi.getBannerApi , {} ,function(res){
            loadBanner.renderBanner(res);
        });
    },
    renderBanner: (res) =>{
        var str = '';
        for(i=0,len = res.data.length ; i < len ; i++){
            str += `<div class="swiper-slide">
                        <img class="swiper-img bannerImg" src="${res.data[i].image}">
                     </div>`;
        }
        $('.swiper-wrapper').empty().append(str);
        renderBanner();
    }
};
loadBanner.getBannerData();
