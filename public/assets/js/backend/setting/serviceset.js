define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'setting/serviceset/index',
                    add_url: 'setting/serviceset/add',
                    edit_url: 'setting/serviceset/edit',
                    del_url: 'setting/serviceset/del',
                    multi_url: 'setting/serviceset/multi',
                    table: 'service_set',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'service_list_name', title: __('Service_list_name')},
                        {field: 'qq_1', title: __('Qq_1')},
                        {field: 'qq_2', title: __('Qq_2')},
                        {field: 'hotline_name', title: __('Hotline_name')},
                        {field: 'hotline_1', title: __('Hotline_1')},
                        {field: 'hotline_2', title: __('Hotline_2')},
                        {field: 'qrcode_image', title: __('Qrcode_image'), formatter: Table.api.formatter.image},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});