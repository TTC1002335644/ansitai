<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use \think\Route;

Route::get('index','web/index/index');
Route::get('dynamic','web/index/dynamic');
Route::get('law','web/index/law');
Route::get('lawDtl','web/index/lawDtl');
Route::get('service','web/index/service');
Route::get('contentShow','web/index/contentShow');
Route::get('contentList','web/index/contentList');
Route::get('footer','web/Common/footer');
//Route::get('qqService','web/index/qqService');

Route::get('login','/admin.php/index/login?url=%2Fadmin.php');



Route::group('api',function(){
    Route::post('getMenuApi','web/webmenu/getMenuApi');//菜单
    Route::post('getBannerApi','web/banner/getBannerApi');//轮播图
    Route::post('getAboutApi','web/about/getAboutApi');//公司简介
    Route::post('getFriendApi','web/friend/getFriendApi');//友情链接
    Route::post('getContactApi','web/contact/getContactApi');//联系我们
    Route::post('getLogoApi','web/ConfigController/getLogoApi');//logo
    Route::post('getBeianApi','web/ConfigController/getBeianApi');//备案
    Route::post('getLawListApi','web/Law/getLawListApi');//获取法律列表
    Route::post('getLawApi','web/Law/getLawApi');//获取法律详情
    Route::post('getSeriveApi','web/Service/getSeriveApi');//获取服务项目
    Route::post('getDynamicApi','web/Dynamic/getDynamicApi');//获取监管动态
    Route::post('getIndexServiceApi','web/Service/getSeriveApi');//获取首页服务项目
    Route::post('getArticleListApi','web/Article/getArticleListApi');//首页文章列表
    Route::post('getArticleShowApi','web/Article/getArticleShowApi');//文章内容
    Route::post('getArticleListByTypeApi','web/Article/getArticleListByTypeApi');//文章列表
    Route::post('getAboutDetailApi','web/About/getAboutDetailApi');//获取关于我们的内容
    Route::post('getContactDetailApi','web/Contact/getContactDetailApi');//获取联系我们内容

    Route::post('getServiceInfoApi','web/Serviceset/getServiceInfoApi');//获取客服信息
});

return [
    //别名配置,别名只能是映射到控制器且访问时必须加上请求的方法
    '__alias__'   => [
    ],
    //变量规则
    '__pattern__' => [
    ],
//        域名绑定到模块
//        '__domain__'  => [
//            'admin' => 'admin',
//            'api'   => 'api',
//        ],
];
