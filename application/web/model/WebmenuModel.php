<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 15:47
 * Author: ttc
 */

namespace app\web\model;

use think\Model;

class WebmenuModel extends Model
{

    protected $name = 'web_menu';

    const STATUS_ON = 1;//显示
    const STATUS_OFF = 2;//不显示


    /*服务项目*/
    public function service(){
        return $this->hasMany(ServiceModel::class , 'pid' , 'id');
    }

    /*监管动态*/
    public function dynamic(){
        return $this->hasOne(DynamicModel::class , 'pid' , 'id');
    }

    public function child(){
        return $this->hasMany(self::class , 'pid' ,'id');
    }

    public function about(){
        return $this->hasOne(AboutModel::class , 'pid' , 'id');
    }

}