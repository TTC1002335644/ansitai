<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/13
 * Time: 13:56
 * Author: ttc
 */

namespace app\web\model;


use think\Model;

class ArticleModel extends Model
{

    protected $name = 'article';

    const STATUS_ON = 1;//显示
    const STATUS_OFF = 2;//不显示

    const TYPE_ONE = 1;//公司动态
    const TYPE_TWO = 2;//行业新闻
    const TYPE_THREE = 3;//培训计划
    const TYPE_FOUR = 4;//客户案例



}