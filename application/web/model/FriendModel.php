<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 17:39
 * Author: ttc
 */

namespace app\web\model;


use think\Model;

class FriendModel extends Model
{

    protected $name = 'friend';

    const STATUS_ON = 1;//上架
    const STATUS_OFF = 2;//下架

}