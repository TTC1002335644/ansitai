<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 16:03
 * Author: ttc
 */

namespace app\web\model;

use think\Model;

class AboutModel extends Model
{
    protected $name = 'about';

    public function hasmenu(){
        return $this->hasOne(WebmenuModel::class , 'id' , 'pid');
    }

}