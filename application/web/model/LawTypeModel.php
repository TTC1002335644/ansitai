<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 09:22
 * Author: ttc
 */

namespace app\web\model;


use think\Model;

class LawTypeModel extends Model
{

    protected $name = 'law_type';

    const STATUS_ON = 1;//显示
    const STATUS_OFF = 2;//不显示

    public function hasmanylaw(){
        return $this->hasMany(LawModel::class , 'law_type_id' , 'id');
    }

}