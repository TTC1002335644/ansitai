<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 17:28
 * Author: ttc
 */

namespace app\web\model;


use think\Model;

class DynamicModel extends Model
{

    protected $name = 'dynamic';

    const STATUS_ON = 1;//显示
    const STATUS_OFF = 2;//不显示

}