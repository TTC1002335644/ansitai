<?php


/**
 * 获取seo信息
 */
if(!function_exists('get_seo')){
	function get_seo(){
		$res = db('seo')->order('id desc')->find();
		return $res;
	}
}

if(!function_exists('strip')) {
    function strip($str)
    {
        $str = str_replace("<br>", "", $str);
        return strip_tags($str);
    }
}
