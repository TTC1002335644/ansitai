<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 19:09
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\ConfigModel;
use think\Controller;

class ConfigController extends Controller
{
    protected $model;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new ConfigModel();
    }

    public function getLogoApi(){
        $condition = ['name' => 'logo' ,'type' =>'image'];
        $res = $this->model->where($condition)->find();
        return JsonReturn(1,'',$res);
    }

    public function getBeianApi(){
        $condition1 = ['name' => 'beian' ,'type' =>'string'];
        $res1 = $this->model->where($condition1)->find();

        $condition2 = ['name' => 'beian_url' ,'type' =>'string'];
        $res2 = $this->model->where($condition2)->find();

        $res = [
            'beian' => $res1,
            'beian_url' => $res2,
        ];
        return JsonReturn(1,'',$res);
    }

}