<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/9
 * Time: 18:16
 * Author: ttc
 */
namespace app\web\controller;

use think\Controller;


class Index extends Controller
{
    protected $seo;

    public function _initialize(){
        parent::_initialize();
        $this->seo = get_seo();

        $menuId = input('menu_id');
        $name = '';
        if(!empty($menuId)){
            $res = db('web_menu')->where('id' , $menuId)->field(['name'])->find();
            if(!empty($res)){
                $name = $res['name'];
            }
        }
        if(!empty($name)){
            $this->seo['title'] = $name.'-'.$this->seo['title'];
        }
        $this->assign('seo',$this->seo);
    }

    public function index(){
        $Common = new Common();
        $this->assign('header',$Common->header());//顶部
        $this->assign('service',$Common->service());//顶部
        $this->assign('firend',$Common->friend());//友情练级
        $this->assign('about',$Common->about());//关于我们
        $this->assign('article',$Common->article());//文章列表
        $this->assign('res',$Common->footer());//底部
        return view('index');
    }

    public function dynamic(){
        $Common = new Common();
        $Dynamic = new Dynamic();
        $this->assign('header',$Common->header());//顶部
        $this->assign('res',$Common->footer());//底部
        $this->assign('data',$Dynamic->getDynamic());//内容
        return view('dynamic');
    }

    public function law(){
        $Common = new Common();
        $Law = new Law();
        $this->assign('header',$Common->header());//顶部
        $this->assign('res',$Common->footer());//底部
        $this->assign('data',$Law->getLawList());//列表
        return view('law');
    }


    public function lawDtl(){
        $Common = new Common();
        $Law = new Law();
        if(input('id')){
            $res = db('law')->where('id',input('id'))->field('title')->find();
            if(!empty($res)){
                $this->seo['title'] = $res['title'].'-'.$this->seo['title'];
                $this->assign('seo',$this->seo);
            }
        }

        $this->assign('header',$Common->header());//顶部
        $this->assign('res',$Common->footer());//底部
        $this->assign('data',$Law->getLaw());//内容
        return view('lawDtl');
    }

    public function service(){
        $Common = new Common();
        $Servce = new Service();
        $this->assign('header',$Common->header());//顶部
        $this->assign('res',$Common->footer());//底部

        $this->assign('data',$Servce->getSerive());//内容
        return view('service');
    }

    public function contentShow($type_name = 'article'){
        $Common = new Common();
        if(input('article_id') && empty(input('menu_id'))){
            $res = db('article')->where('id',input('article_id'))->field('title')->find();
            if(!empty($res)){
                $this->seo['title'] = $res['title'].'-'.$this->seo['title'];
                $this->assign('seo',$this->seo);
            }
        }

        $this->assign('header',$Common->header());//顶部
        $this->assign('res',$Common->footer());//底部

        $this->assign('typeName',$type_name);
        if($type_name == 'article'){
            $this->assign('article',$Common->getArticleShow());
        }elseif($type_name == 'about'){
            $result = $Common->getAboutDetail(input('article_id'));
            if(!empty($result)){
                $this->seo['keywords'] = strip($result['detail']->depict);
                $this->seo['description'] = strip($result['detail']->depict);
                $this->assign('seo',$this->seo);
            }
            $this->assign('article',$result);
        }elseif($type_name == 'contact'){
            $this->assign('article',$Common->getContactDetail());
        }
        return view('contentShow');
    }

    public function contentList(){
        $Common = new Common();
        $Articl = new Article();
        $this->assign('header',$Common->header());//顶部
        $this->assign('res',$Common->footer());//底部
        $this->assign('data',$Articl->getArticleListByType());//列表
        return view('contentList');
    }

    public function qqService(){
        return view('ast');
    }


}