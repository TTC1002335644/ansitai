<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 18:33
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\WebmenuModel;
use think\Controller;

class Webmenu extends Controller
{

    protected $mdoel = null;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new WebmenuModel();
    }



    public function getMenuApi(){
        $condition = [
            'status' => WebmenuModel::STATUS_ON,
            'pid' => 0
        ];

        $res = $this->model
            ->where($condition)
            ->with([
                'child' => function ($query) use($condition){
                    $query->where($condition)
                        ->with([
                            'about'
                        ])
                        ->order('sort','asc');
                }
            ])
            ->order('sort','asc')
            ->select();
        return JsonReturn(1,'',$res);
    }

}