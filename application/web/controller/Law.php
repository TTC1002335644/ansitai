<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 09:15
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\LawModel;
use app\web\model\LawTypeModel;
use think\Controller;

class Law extends Controller
{

    protected $model = null;
    protected $lawTypeModel = null;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new LawModel();
        $this->lawTypeModel = new LawTypeModel();
    }


    public function getLawListApi(){
        $condition = ['status' => LawTypeModel::STATUS_ON];

        $menuList = $this->lawTypeModel
            ->where($condition)
            ->with([
                'hasmanylaw' => function ($query){
                    $query->order('sort','asc');
                }
            ])
            ->order('sort','asc')
            ->select();

        return JsonReturn(1,'',$menuList);
    }

    public function getLawList(){
        $condition = ['status' => LawTypeModel::STATUS_ON];

        $menuList = $this->lawTypeModel
            ->where($condition)
            ->with([
                'hasmanylaw' => function ($query){
                    $query->order('sort','asc');
                }
            ])
            ->order('sort','asc')
            ->select();

        return $menuList;
    }


    public function getLawApi(int $id = 1 , bool $getList = true){
        $condition = ['id' => $id];

        $data['detail'] = $this->model
            ->where($condition)
            ->order('sort','asc')
            ->find();
        if($getList == true && !empty($data['detail'])){
            $data['list'] = $this->getNextAndLast($data['detail']['id']);
        }
        return JsonReturn(1,'',$data);
    }

    public function getLaw(int $id = 1 , bool $getList = true){
        $condition = [
            'id' => input('id') ?? $id
        ];

        $data['detail'] = $this->model
            ->where($condition)
            ->order('sort','asc')
            ->find();
        if($getList == true && !empty($data['detail'])){
            $data['list'] = $this->getNextAndLast($data['detail']['id']);
        }
        return $data;
    }

    /**
     * 获取上一条数据和下一条数据
     */
    public function getNextAndLast(int $id = 0){
        $next = $this->model->where('id','>',$id)->order('sort','asc')->find();
        $last = $this->model->where('id','<',$id)->order('sort','asc')->find();
        if(empty($next)){
            $next = [
                'id' => 0,
                'title' => '无',
            ];
        }

        if(empty($last)){
            $last = [
                'id' => 0,
                'title' => '无',
            ];
        }
        return ['next' => $next , 'last' => $last];
    }



}