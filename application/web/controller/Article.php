<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/13
 * Time: 13:55
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\ArticleModel;
use think\Controller;

class Article extends Controller
{

    protected $model = null;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new ArticleModel();
    }


    /**
     * 列表
     * */
    public function getArticleListApi(int $size = 3){
        $condition['status'] = ArticleModel::STATUS_ON;
        $arr = [ArticleModel::TYPE_ONE ,ArticleModel::TYPE_TWO ,ArticleModel::TYPE_THREE];
        foreach ($arr as $v){
            $res[] = $this->model
                ->where($condition)
                ->where('type',$v)
                ->order('sort asc , id desc')
                ->limit($size)
                ->select();
        }
        return JsonReturn(1,'',$res);
    }

    /**
     * 列表（分类）
     * */
    public function getArticleListByTypeApi(int $type = 1 , int $size = 100){
        $condition = [
            'status' => ArticleModel::STATUS_ON,
            'type' => $type
        ];
        $res = $this->model
            ->where($condition)
            ->order('sort asc , id desc')
            ->limit($size)
            ->select();

        return JsonReturn(1,'',$res);
    }


    /**
     * 详情
     * */
    public function getArticleShowApi(){
        $articleId = input('article_id');
        $condition = ['status' => ArticleModel::STATUS_ON , 'id' => $articleId];
        $res['detail'] = $this->model->where($condition)->find();
        if(!empty($res['detail'])){
            $res['list'] = $this->getNextAndLast($res['detail']['id']);
        }
        return JsonReturn(1,'',$res);
    }


    /**
     * 获取上一条数据和下一条数据
     */
    public function getNextAndLast(int $id = 0){
        $next = $this->model->where('id','>',$id)->order('sort','asc')->find();
        $last = $this->model->where('id','<',$id)->order('sort','asc')->find();
        if(empty($next)){
            $next = [
                'id' => 0,
                'title' => '无',
            ];
        }

        if(empty($last)){
            $last = [
                'id' => 0,
                'title' => '无',
            ];
        }
        return ['next' => $next , 'last' => $last];
    }


    /**
     * 列表
     * */
    public function getArticleListByType(int $type = 1 , int $size = 100){
        $condition = [
            'status' => ArticleModel::STATUS_ON,
            'type' => input('type') ??$type
        ];
        $res = $this->model
            ->where($condition)
            ->order('sort asc , id desc')
            ->limit($size)
            ->select();


        return [
            'data' => $res,
            'typeArr' => [
                '1' => [
                    'name'=>'公司动态'
                ],
                '2' => [
                    'name'=>'行业新闻'
                ],
                '3' => [
                    'name'=>'培训计划',
                ],
                '4' => [
                    'name'=>'客户案例'
                ],
            ]
        ];
    }


}