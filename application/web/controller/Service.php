<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 14:36
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\ServiceModel;
use app\web\model\WebmenuModel;
use think\Controller;

class Service extends Controller
{

    protected $model = null;

    protected $webmenu = null;

    protected $service_bind_id;

    public function _initialize()
    {
        $this->model = new ServiceModel();
        $this->webmenu = new WebmenuModel();

        $this->service_bind_id = (getConfig('service_bind_id'))['value'];
    }

    public function getSeriveApi(){
        $condition = [
            'pid' => $this->service_bind_id,
            'status' => WebmenuModel::STATUS_ON
        ];
        $res = $this->webmenu
        ->where($condition)
        ->with([
            'service' => function ($query){
                $query->where(['status' => ServiceModel::STATUS_ON])->order('sort','asc');
            }
        ])
        ->order('sort','asc')
        ->select();

        return JsonReturn(1,'',$res);
    }


    public function getSerive(){
        $condition = [
            'pid' => $this->service_bind_id,
            'status' => WebmenuModel::STATUS_ON
        ];
        $res = $this->webmenu
            ->where($condition)
            ->with([
                'service' => function ($query){
                    $query->where(['status' => ServiceModel::STATUS_ON])->order('sort','asc');
                }
            ])
            ->order('sort','asc')
            ->select();
        return $res;
    }














}