<?php
namespace app\web\controller;


use app\web\model\AboutModel;
use app\web\model\ArticleModel;
use app\web\model\ConfigModel;
use app\web\model\ContactModel;
use app\web\model\FriendModel;
use app\web\model\ServiceModel;
use app\web\model\WebmenuModel;
use app\web\controller\Article;
use think\Controller;

class Common extends Controller{

    protected $service_bind_id;

    protected function _initialize()
    {
        parent::_initialize();
        $this->service_bind_id = (getConfig('service_bind_id'))['value'];
    }

    /**
     * 头部
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function header(){
        $condition = ['name' => 'logo' ,'type' =>'image'];
        $logo = (new ConfigModel())->where($condition)->find();

        $condition = [
            'status' => WebmenuModel::STATUS_ON,
            'pid' => 0
        ];

        $menu = (new WebmenuModel())
            ->where($condition)
            ->with([
                'child' => function ($query) use($condition){
                    $query->where($condition)
                        ->with([
                            'about'
                        ])
                        ->order('sort','asc');
                }
            ])
            ->order('sort','asc')
            ->select();

        return [
            'logo'=>$logo,
            'menu'=>$menu
        ];

    }



    /**
     * 友情链接
     * @param int $page
     * @param int $len
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function friend($page = 1 , $len = 100){
        $condition = ['status' => FriendModel::STATUS_ON];
        $res = (new FriendModel())
            ->where($condition)
            ->order('sort','ASE')
            ->page($page,$len)
            ->select();
        return $res;
    }

    /**
     * 关于我们
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function about(){
        $condition = [];
        $res = (new AboutModel())
            ->where($condition)
            ->with([
                'hasmenu' => function ($query){
                    $query->field(['id','name','url']);
                }
            ])
            ->order('id','ASE')
            ->select();

        //获取联系我们的url
        $contact_bind_id = getConfig('contact_bind_id');
        $contactRes = (new WebmenuModel())->where([
            'id' => $contact_bind_id['value']
        ])->find();
        return [
            'data' => $res,
            'contact_res' => $contactRes
        ];
    }

    /**
     * 首页文章列表
     * @param int $size
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function article(int $size = 3){
        $condition['status'] = ArticleModel::STATUS_ON;
        $arr = [ArticleModel::TYPE_ONE ,ArticleModel::TYPE_TWO ,ArticleModel::TYPE_THREE];
        foreach ($arr as $v){
            $res[] = (new ArticleModel())
                ->where($condition)
                ->where('type',$v)
                ->order('sort asc , id desc')
                ->limit($size)
                ->select();
        }
        return [
            'data' => $res,
            'typeArr' => [
                '1' => [
                    'name'=>'公司动态'
                ],
                '2' => [
                    'name'=>'行业新闻'
                ],
                '3' => [
                    'name'=>'培训计划',
                    ],
                '4' => [
                    'name'=>'客户案例'
                ],
            ]
        ];
    }


    /**
     * 首页服务项目
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function service(){
        $condition = [
            'pid' => $this->service_bind_id,
            'status' => WebmenuModel::STATUS_ON
        ];
        $res = (new WebmenuModel())
            ->where($condition)
            ->with([
                'service' => function ($query){
                    $query->where(['status' => ServiceModel::STATUS_ON])->order('sort','asc');
                }
            ])
            ->order('sort','asc')
            ->select();
        return $res;
    }


    /**
     * 底部
     */
    public function footer(){
        $condition = [];
        $condition1 = ['name' => 'beian' ,'type' =>'string'];
        $ConfigModel = new ConfigModel();
        $ContactModel = new ContactModel();

        $res1 = $ConfigModel->where($condition1)->find();

        $condition2 = ['name' => 'beian_url' ,'type' =>'string'];
        $res2 = $ConfigModel->where($condition2)->find();

        //联系地址等等信息
        $res3 = $ContactModel
            ->where($condition)
            ->order('id','ASE')
            ->find();

        $res = [
            'beian' => $res1,
            'beian_url' => $res2,
            'data' => $res3,
        ];

        return $res;
    }


    /**
     * 文章详情
     * */
    public function getArticleShow(){
        $articleId = input('article_id');
        $condition = ['status' => ArticleModel::STATUS_ON , 'id' => $articleId];
        $res['detail'] = (new ArticleModel())->where($condition)->find();
        if(!empty($res['detail'])){
            $ArticleController = new Article();
            $res['list'] = $ArticleController->getNextAndLast($res['detail']['id']);
        }
        return $res;
    }



    /**
     * 关于我们详情
     * @param int $article_id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAboutDetail(int $article_id = 1){
        $condition = [
            'id' => $article_id
        ];
        $res['detail'] = (new AboutModel())
            ->where($condition)
            ->with([
                'hasmenu' => function ($query){
                    $query->field(['id','name']);
                }
            ])
            ->find();
        return $res;
    }

    /**
     * 获取联系我们
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getContactDetail(){
        $condition = [];
        //联系地址等等信息
        $res['detail'] = (new ContactModel())
            ->where($condition)
            ->order('id','ASE')
            ->find();
        return $res;
    }


}