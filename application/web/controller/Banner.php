<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 10:41
 * Author: ttc
 */

namespace app\web\controller;

use app\web\model\BannerModel;
use think\Controller;

class Banner extends Controller
{

    protected $bannerModel;

    public function _initialize()
    {
        parent::_initialize();
        $this->bannerModel = new BannerModel();
    }
    /**
     * 获取banner的api
     * @return \think\response\Json
     */
    public function getBannerApi(){
        $condition = ['status' => BannerModel::STATUS_ON];
        $res = $this->bannerModel
            ->where($condition)
            ->order('sort','ASE')
            ->select();
        return JsonReturn(1,'',$res);
    }


}