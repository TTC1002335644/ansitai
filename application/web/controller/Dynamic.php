<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/12
 * Time: 17:28
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\DynamicModel;
use app\web\model\WebmenuModel;
use think\Controller;

class Dynamic extends Controller
{

    protected $model = null;
    protected $webmenu = null;
    protected $dynamic_bind_id;



    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new DynamicModel();
        $this->webmenu = new WebmenuModel();
        $this->dynamic_bind_id = (getConfig('dynamic_bind_id'))['value'];
    }

    public function getDynamicApi(){
        $condition = [
            'pid' => $this->dynamic_bind_id,
            'status' => WebmenuModel::STATUS_ON
        ];
        $res = $this->webmenu
            ->where($condition)
            ->with([
                'dynamic' => function ($query){
                    $query->where(['status' => DynamicModel::STATUS_ON])->order('id','desc');
                }
            ])
            ->order('sort','asc')
            ->select();

        return JsonReturn(1,'',$res);
    }

    public function getDynamic(){
        $condition = [
            'pid' => $this->dynamic_bind_id,
            'status' => WebmenuModel::STATUS_ON
        ];
        $res = $this->webmenu
            ->where($condition)
            ->with([
                'dynamic' => function ($query){
                    $query->where(['status' => DynamicModel::STATUS_ON])->order('id','desc');
                }
            ])
            ->order('sort','asc')
            ->select();

        return $res;
    }



}