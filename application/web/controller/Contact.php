<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 18:23
 * Author: ttc
 */

namespace app\web\controller;

use app\web\model\ContactModel;
use think\Controller;

class Contact extends Controller
{

    protected $model;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ContactModel();
    }


    public function getContactApi(){
        $condition = [];
        //联系地址等等信息
        $res = $this->model
            ->where($condition)
            ->order('id','ASE')
            ->find();
        return JsonReturn(1,'',$res);
    }

    public function getContactDetailApi(){
        $condition = [];
        //联系地址等等信息
        $res['detail'] = $this->model
            ->where($condition)
            ->order('id','ASE')
            ->find();
        return JsonReturn(1,'',$res);
    }


}