<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 17:39
 * Author: ttc
 */

namespace app\web\controller;

use app\web\model\FriendModel;
use think\Controller;

class Friend extends Controller
{

    protected $model;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new FriendModel();
    }


    public function getFriendApi($page = 1 , $len = 100){
        $condition = ['status' => FriendModel::STATUS_ON];
        $res = $this->model
            ->where($condition)
            ->order('sort','ASE')
            ->page($page,$len)
            ->select();
        return JsonReturn(1,'',$res);
    }



}