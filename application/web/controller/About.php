<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/11
 * Time: 16:02
 * Author: ttc
 */

namespace app\web\controller;

use app\web\model\AboutModel;
use app\web\model\WebmenuModel;
use think\Controller;

class About extends Controller
{

    protected $model;
    protected $WebmenuModel;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new AboutModel();
        $this->WebmenuModel = new WebmenuModel();

    }


    public function getAboutApi(){
        $condition = [];
        $res = $this->model
            ->where($condition)
            ->with([
                'hasmenu' => function ($query){
                    $query->field(['id','name','url']);
                }
            ])
            ->order('id','ASE')
            ->select();

        //获取联系我们的url
        $contact_bind_id = getConfig('contact_bind_id');
        $contactRes = $this->WebmenuModel->where([
            'id' => $contact_bind_id['value']
        ])->find();


        return JsonReturn(1,'',$res ,$contactRes);
    }


    /*
     * 详情
     * */
    public function getAboutDetailApi(int $article_id = 1){
        $condition = [
            'id' => $article_id
        ];
        $res['detail'] = $this->model
            ->where($condition)
            ->with([
                'hasmenu' => function ($query){
                    $query->field(['id','name']);
                }
            ])
            ->find();
        return JsonReturn(1,'',$res);
    }


}