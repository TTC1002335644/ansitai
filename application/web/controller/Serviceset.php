<?php
/**
 * Created by PhpStorm.
 * User: sr-php
 * Date: 2019/4/18
 * Time: 11:18
 * Author: ttc
 */

namespace app\web\controller;


use app\web\model\ServicesetModel;
use think\Controller;

class Serviceset extends Controller
{

    protected $model = null;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new ServicesetModel();
    }

    /**
     * 获取客服信息
     * @return \think\response\Json
     */
    public function getServiceInfoApi(){
        $condition = [];
        $res = $this->model
            ->where($condition)
            ->order('id','desc')
            ->find();

        return JsonReturn(1,'',$res);
    }

}