<?php

return [
    'Id'  =>  'ID',
    'Service_list_name'  =>  '标题',
    'Qq_1'  =>  'QQ号码一',
    'Qq_2'  =>  'QQ号码二',
    'Hotline_name'  =>  '热线名字',
    'Hotline_1'  =>  '热线一',
    'Hotline_2'  =>  '热线二',
    'Qrcode_image'  =>  '二维码',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
