<?php

return [
    'Id'  =>  'ID',
    'Title'  =>  'SEO标题',
    'Keywords'  =>  'SEO关键字',
    'Description'  =>  'SEO描述',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
