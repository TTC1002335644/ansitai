<?php

return [
    'Id'  =>  'ID',
    'Name'  =>  '名字',
    'Pid'  =>  '主菜单',
    'Content'  =>  '内容',
    'Status'  =>  '状态',
    'Status 1'  =>  '显示',
    'Status 2'  =>  '不显示',
    'Sort'  =>  '排序',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
