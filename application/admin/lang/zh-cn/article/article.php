<?php

return [
    'Id'  =>  'ID',
    'Type'  =>  '所属栏目',
    'Type 1'  =>  '公司动态',
    'Type 2'  =>  '行业新闻',
    'Type 3'  =>  '培训计划',
    'Type 4'  =>  '客户案例',
    'Title'  =>  '标题',
    'Depict'  =>  '描述',
    'Sort'  =>  '排序',
    'Showtime'  =>  '所展示时间',
    'Status'  =>  '状态',
    'Status 1'  =>  '显示',
    'Status 2'  =>  '不显示',
    'Content'  =>  '内容',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
