<?php

return [
    'Id'  =>  'ID',
    'Law_type_id'  =>  '分类',
    'Source_name'  =>  '来源',
    'Author'  =>  '作者',
    'Image'  =>  '图片',
    'Title'  =>  '标题',
    'Content'  =>  '内容',
    'Introduct'  =>  '简介',
    'Sort'  =>  '排序',
    'Showtime'  =>  '显示时间',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
