<?php

return [
    'Id'  =>  'ID',
    'Pid'  =>  '菜单',
    'Image'  =>  '图片',
    'Depict'  =>  '描述',
    'Content'  =>  '内容',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
