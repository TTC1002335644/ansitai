<?php

return [
    'Id'  =>  'ID',
    'Name'  =>  '名字',
    'Url'  =>  '链接',
    'Icon'  =>  '图片',
    'Sort'  =>  '排序',
    'Status'  =>  '状态',
    'Status 1'  =>  '显示',
    'Status 2'  =>  '不显示',
    'Pid'  =>  '上级菜单',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '修改时间'
];
