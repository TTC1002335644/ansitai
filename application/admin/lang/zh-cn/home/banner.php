<?php

return [
    'Id'  =>  'ID',
    'Image'  =>  '图片',
    'Jump_url'  =>  '跳转链接',
    'Status'  =>  '状态',
    'Status 1'  =>  '显示',
    'Status 2'  =>  '不显示',
    'Sort'  =>  '排序',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
