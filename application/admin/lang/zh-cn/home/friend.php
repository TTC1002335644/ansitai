<?php

return [
    'Id'  =>  'ID',
    'Image'  =>  '图片',
    'Url'  =>  '跳转链接',
    'Sort'  =>  '排序',
    'Status'  =>  '状态',
    'Status 1'  =>  '上架',
    'Status 2'  =>  '下架',
    'Remark'  =>  '备注',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
