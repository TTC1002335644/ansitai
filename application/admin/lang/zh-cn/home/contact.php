<?php

return [
    'Tel'  =>  '电话',
    'Address'  =>  '地址',
    'Fax'  =>  '传真',
    'Web_site'  =>  '网址',
    'Zip_code'  =>  '邮箱',
    'Qrcode'  =>  '底部二维码',
    'Content'  =>  '内容',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
