<?php

return [
    'Id'  =>  'ID',
    'Pid'  =>  '所属菜单',
    'Remark'  =>  '备注',
    'Status'  =>  '状态',
    'Status 1'  =>  '显示',
    'Status 2'  =>  '不显示',
    'Content'  =>  '内容',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
