<?php

namespace app\admin\model;

use think\Model;
use app\admin\model\web\Menu;

class About extends Model
{
    // 表名
    protected $name = 'about';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
    // 追加属性
    protected $append = [

    ];

    public function hasmenu(){
        return $this->hasOne(Menu::class , 'id' , 'pid');
    }
    

    







}
