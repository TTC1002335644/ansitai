<?php

namespace app\admin\model;

use think\Model;

class Law extends Model
{
    // 表名
    protected $name = 'law';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
    // 追加属性
    protected $append = [
        'showtime_text'
    ];
    

    



    public function getShowtimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['showtime']) ? $data['showtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setShowtimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    public function lawtype(){
        return $this->hasOne('app\admin\model\law\Type', 'id','law_type_id')->setEagerlyType(0)->field('id,name');
    }


}
