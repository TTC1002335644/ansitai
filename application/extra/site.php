<?php

return array (
  'name' => '安思泰',
  'web_site' => 'http://ast.seedian.com.cn',
  'beian' => '版权所有2019：广州安思泰企业管理咨询有限公司 COPYRIGHT 2011 ANSINDAR.COM ALL RIGHTS RESERVED.',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'menuconfig' => '菜单其他配置',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'cdnurl' => '',
  'logo' => '/uploads/20190411/e05a02dab9d60206709ff4284fcfc5f3.png',
  'service_bind_id' => '3',
  'dynamic_bind_id' => '5',
  'about_bind_id' => '2',
  'contact_bind_id' => '8',
);